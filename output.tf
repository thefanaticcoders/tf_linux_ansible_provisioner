output "id" {
  value = "azurerm_public_ip.example_public_ip.id"
}

output "ip_address" {
  value = "azurerm_public_ip.example_public_ip.ip_address"
}

