# Create a resource group
resource "azurerm_resource_group" "example_rg" {
  name     = "${var.resource_prefix}-RG"
  location = var.ansiblenode_location
}

# Create a virtual network within the resource group
resource "azurerm_virtual_network" "example_vnet" {
  name                = "${var.resource_prefix}-vnet"
  resource_group_name = azurerm_resource_group.example_rg.name
  location            = var.ansiblenode_location
  address_space       = [var.ansiblenode_address_space]
}


# Create a subnets within the virtual network
resource "azurerm_subnet" "example_subnet" {
  name                 = "${var.resource_prefix}-subnet"
  resource_group_name  = azurerm_resource_group.example_rg.name
  virtual_network_name = azurerm_virtual_network.example_vnet.name
  address_prefix       = var.ansiblenode_address_prefix
}

# Create Linux Public IP 
resource "azurerm_public_ip" "example_public_ip" {
  name                = "${var.resource_prefix}-PublicIP"
  location            = azurerm_resource_group.example_rg.location
  resource_group_name = azurerm_resource_group.example_rg.name
  allocation_method   = var.Environment == "Test" ? "Static" : "Dynamic"
  tags = {
    environment = "Test"
  }
}

# Create Network Interface
resource "azurerm_network_interface" "example_nic" {
  name = "${var.resource_prefix}-NIC"
  #name                = "${var.resource_prefix}-${format("%02d", count.index)}-NIC"
  location            = azurerm_resource_group.example_rg.location
  resource_group_name = azurerm_resource_group.example_rg.name
  #count               = var.ansiblenode_count

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.example_subnet.id
    private_ip_address_allocation = "Dynamic"
    #public_ip_address_id          = azurerm_public_ip.example_public_ip[count.index].id
    #public_ip_address_id          = azurerm_public_ip.example_public_ip.id
    public_ip_address_id = azurerm_public_ip.example_public_ip.id
  }
}


# Creating resource NSG
resource "azurerm_network_security_group" "example_nsg" {

  name                = "${var.resource_prefix}-NSG"
  location            = azurerm_resource_group.example_rg.location
  resource_group_name = azurerm_resource_group.example_rg.name

  # Security rule can also be defined with resource azurerm_network_security_rule, here just defining it inline. 
  security_rule {
    name                       = "Inbound"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"

  }
  tags = {
    environment = "Test"
  }
}

# Subnet and NSG association 
resource "azurerm_subnet_network_security_group_association" "example_subnet_nsg_association" {
  subnet_id                 = azurerm_subnet.example_subnet.id
  network_security_group_id = azurerm_network_security_group.example_nsg.id

}

#data "azurerm_public_ip" "example_public_ip" {
# name                = azurerm_public_ip.example_public_ip.name
# resource_group_name = azurerm_public_ip.example_public_ip.resource_group_name
#}

# Virtual Machine Creation - Linux
resource "azurerm_virtual_machine" "example_linux_vm" {
  #name                          = "${var.resource_prefix}-${format("%02d", count.index)}"
  name                          = "${var.resource_prefix}-VM"
  location                      = azurerm_resource_group.example_rg.location
  resource_group_name           = azurerm_resource_group.example_rg.name
  network_interface_ids         = [azurerm_network_interface.example_nic.id]
  vm_size                       = "Standard_A1_v2"
  delete_os_disk_on_termination = true
  #count                         = var.ansiblenode_count

  storage_image_reference {
    publisher = "OpenLogic"
    offer     = "CentOS"
    sku       = "7.5"
    version   = "latest"
  }
  storage_os_disk {
    name              = "myosdisk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  os_profile {
    computer_name  = "ansiblehost"
    admin_username = var.usr
    admin_password = var.pwd
  }
  os_profile_linux_config {
    disable_password_authentication = false
  }

  provisioner "remote-exec" {
    inline = [
      "echo ${var.pwd} | sudo -S yum install epel-release -y",
      "sudo yum install ansible -y",
      "sudo yum install python-pip -y",
      "sudo yum install java-1.8.0-openjdk-devel -y",
      "sudo curl --silent --location http://pkg.jenkins-ci.org/redhat-stable/jenkins.repo | sudo tee /etc/yum.repos.d/jenkins.repo",
      "sudo rpm --import https://jenkins-ci.org/redhat/jenkins-ci.org.key",
      "sudo yum install jenkins --nogpgcheck -y && sudo systemctl start jenkins && sudo systemctl enable jenkins"
    ]

    connection {
      type     = "ssh"
      user     = var.usr
      password = var.pwd
      #host     = data.azurerm_public_ip.example_public_ip.ip_address
      host = azurerm_public_ip.example_public_ip.ip_address
    }

  }

  tags = {
    environment = "Test"
  }
}